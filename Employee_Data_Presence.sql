Select SPRIDEN_ID,e.PIDM, SPRIDEN_FIRST_NAME,SPRIDEN_LAST_NAME,EWUAPP.F_GET_EMAIL(SPRIDEN_PIDM, 'A','EMP','PERS'),'Faculty/Staff'
        from EWUAPP.NZVEMPL e
        left join SPRIDEN ON SPRIDEN_PIDM = e.PIDM
        where (((e.IS_ACTIVE='Y' AND e.IS_STAFF ='Y')  OR (e.IS_ACTIVE='Y' AND e.IS_FACULTY='Y'))
        AND e.DAYS_SINCE_PAID<60 AND SPRIDEN_CHANGE_IND is null
        AND ( e.ECLS1 !='R' OR e.ECLS1 !='S' OR e.ECLS1 !='G'))
       /* AND e.PIDM not in (select PIDM from EWUAPP.SZFPRSC_PERSON))*/ ;