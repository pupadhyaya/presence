WITH STUDENT_INFO_REC AS(

SELECT NET_ID,
        STUDENT_ID as ewu_id,
        PIDM,
        LAST_NAME,
        FIRST_NAME,
        MIDDLE_INITIAL,
        SEX as GENDER,
        CONFID_FLAG,
        DEAD_IND,
        substr(case
                 when p.spbpers_ethn_cde = '2' then
                  'Hispanic/Latino'
                 when race.race_category_count > 1 then
                  'Two or more races'
                 when race.race_category_count = 1 then
                  race.race_category
                 else
                  'Unknown'
               end,
               1,
               50) as ETHNICITY,
        --stu e-mail address
        ewuapp.f_get_email(i_pidm         => pidm,
                           i_status_ind   => 'A',
                           i_email_code_1 => 'STU',
                           i_email_code_2 => null,
                           i_email_code_3 => null,
                           i_email_code_4 => null) as email_addr,

       -- addr.SPRADDR_ATYP_CODE,
        --addr.SPRADDR_STREET_LINE1,
        --addr.SPRADDR_STREET_LINE2,
       -- addr.SPRADDR_CITY,
       -- addr.SPRADDR_STAT_CODE,
      --  addr.SPRADDR_ZIP,
        --under 18 flag
        case
          when floor(months_between(sysdate,
                                    BIRTH_DATE

                                    ) / 12) < 18 then
           'Y'
          else
           'N'
        end as under_18_IND,
        --Under 21
        case
          when floor(months_between(sysdate,
                                    BIRTH_DATE

                                    ) / 12) < 21 then
           'Y'
          else
           'N'
        end as under_21_IND,
        --veterans ind
        case
          when veterans_code is not null then
           'Y'
          else
           'N'
        end as VETERANS_IND,
        AUAP_STU_IND,
        ELI_STU_IND,
        OGI_STU_IND,
        ON_CAMPUS_RS_IND,
        OFF_CAMPUS_RS_IND,
        CAMP_CODE,
        ADMIT_TYPE_DESC,
        --get admit type code based on description
        --only look for transfer student code like T%
        case
          when (select stvstyp_code
                  from stvstyp
                 where upper(STVSTYP_DESC) = upper(ADMIT_TYPE_DESC)) like
               ('T%') THEN
           'Y'
          else
           'N'
        end as transfer_stu_ind,
        STUDENT_CLASSIFICATION,
        MAJOR_CODE,
        MAJOR_DESC,
        'STUDENT' as entity_type,
        --crd.cardnum as card_num,
        --crd.card_issuenum as card_issue_num,
        STYP_CODE,
        STUDENT_TYPE_DESC,
        LEVEL_CODE,
        LEVEL_CODE_DESC,
        TERM_CODE,
        TRMT_CODE,
        QUARTER_STU_IND,
        SEMESTER_STU_IND,
        RESD_CODE,
        ENROLLED_THIS_TERM,
        REGISTERED_THIS_TERM,
        THIS_TERM_REGSTR_HOURS

   from EWUAPP.SZVSSEL_BY_TERM_1
/* --get address by type
   left join (
              -- Rank the addresses
              select adrr.*,
                      --ADDR_ROW_ID,
                      rank() over(partition by adrr.SPRADDR_PIDM order by adrr.ADDR_IS_ACTIVE DESC, adrr.ADDR_PRIORITY, adrr.spraddr_from_date) as ADDR_RANK
                from (
                       -- Select addresses for this user
                       select spraddr.*,

                               CASE
                                 WHEN i_addr_type_1 IS NOT NULL AND
                                      spraddr_atyp_code = i_addr_type_1 THEN
                                  1
                                 WHEN i_addr_type_2 IS NOT NULL AND
                                      spraddr_atyp_code = i_addr_type_2 THEN
                                  2
                                 WHEN i_addr_type_3 IS NOT NULL AND
                                      spraddr_atyp_code = i_addr_type_3 THEN
                                  3
                                 WHEN i_addr_type_4 IS NOT NULL AND
                                      spraddr_atyp_code = i_addr_type_4 THEN
                                  4
                                 WHEN i_addr_type_5 IS NOT NULL AND
                                      spraddr_atyp_code = i_addr_type_5 THEN
                                  5
                                 ELSE
                                  6
                               END ADDR_PRIORITY,
                               CASE
                                 WHEN trunc(nvl(spraddr_from_date, sysdate - 1)) <=
                                      trunc(sysdate) AND
                                      trunc(nvl(spraddr_to_date, sysdate + 1)) >=
                                      trunc(sysdate) THEN
                                  1
                                 ELSE
                                  0
                               END ADDR_IS_ACTIVE

                         from spraddr
                        where

                        (trunc(nvl(spraddr_from_date, sysdate - 1)) <=
                        trunc(sysdate) AND
                        trunc(nvl(spraddr_to_date, sysdate + 1)) >=
                        trunc(sysdate) AND SPRADDR_STATUS_IND is null)) adrr
               where ADDR_PRIORITY <= 5) addr
     on addr.spraddr_pidm = pidm
    and ADDR_RANK = 1*/
 -------------------------------------------------------------------------
/* --get bbt card number
   left join (SELECT cardnum, MAX(issue_number) as card_issuenum
                FROM envision.card@"ewuapp.blackboard"
              -- card status
              --(0) - Active
              --(1) - Frozen
              --(2) - Retired (cannot be changed to active without utility)
              --(3) - Customer Deleted
               WHERE card_status = 0 --only active
               GROUP BY cardnum) crd
     on to_number(crd.cardnum) = to_number(student_id) --note: join assumes that cardnum is the same as student */
-------------------------------------------------------------------------------
   left JOIN spbpers p
     ON spbpers_pidm = PIDM
   LEFT JOIN stvethn
     ON stvethn_code = p.spbpers_ethn_code
 --race
   left join ( --Inline view for race
              select gorprac_pidm,
                      max(case
                            when gorprac_race_cde = 'R6' then
                             '6'
                            when gorprac_race_cde = 'R7' then
                             '7'
                            else
                             gtvrrac_code
                          end) as race_code,
                      max(case
                            when gorprac_race_cde = 'R6' then
                             'Two or more races'
                            else
                             gtvrrac_desc
                          end) as race_category,
                      count(distinct gtvrrac_code) as race_category_count
                from gorprac
                join gorrace
                  on gorrace_race_cde = gorprac_race_cde

                join gtvrrac
                  on gtvrrac_code = gorrace_rrac_code
                 and gtvrrac_code != '0'
               group by gorprac_pidm) race
     on race.gorprac_pidm = PIDM

  where term_code in ('202030','202035','202040')
  and OFF_CAMPUS_RS_IND='N'
       --student check: registration requirements
/*    and 'Y' = (case
          when trim(i_registered) is null or upper(i_registered) != 'Y' then
           'Y'
        --special population handling
          when upper(i_registered) = 'Y' and
               (AUAP_STU_IND = 'Y' or OGI_STU_IND = 'Y' or ELI_STU_IND = 'Y' or
                OFF_CAMPUS_RS_IND = 'Y') then
           'Y'
          when upper(i_registered) = 'Y' and
               nvl(REGISTERED_THIS_TERM, 'N') = 'Y' THEN
           'Y'
          else
           'N'
        end)*/
       --Exclude active employees but keep student hourly and/or graduate appointment (is_student = 'Y')
       --This change assume that student hourly and/or graduate appointment are registered in classes
    AND PIDM NOT IN (SELECT pidm
                       FROM EWUAPP.NZVEMPL
                      WHERE NVL(upper(is_active), 'N') = 'Y'
                        AND NVL(upper(is_student), 'N') = 'N')
  order by student_id)

SELECT ewu_id,PIDM,LAST_NAME,FIRST_NAME, email_addr FROM STUDENT_INFO_REC where email_addr is not null ;
